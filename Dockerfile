ARG GO_VER
FROM golang:$GO_VER

RUN apt-get update -y -qq && \
    apt-get install -y -qq p7zip-full android-sdk && \
    apt-get clean && \
    echo '#!/bin/bash\njava -jar /usr/lib/android-sdk/build-tools/debian/apksigner.jar "$@"' > /usr/lib/android-sdk/build-tools/debian/apksigner && \
    chmod 755 /usr/lib/android-sdk/build-tools/debian/apksigner && \
    ln -sf /usr/lib/android-sdk/build-tools/debian/apksigner /usr/bin/apksigner

ARG NDK_VER
RUN cd / && \
    wget -q -O ndk.zip https://dl.google.com/android/repository/android-ndk-${NDK_VER}-linux-x86_64.zip && \
    7z x ndk.zip && \
    rm ndk.zip

ENV ANDROID_HOME /usr/lib/android-sdk/
ENV ANDROID_NDK_HOME /android-ndk-${NDK_VER}/

ARG FYNE_VER
RUN go get fyne.io/fyne/v2/cmd/fyne@${FYNE_VER}
